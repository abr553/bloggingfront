import 'v-markdown-editor/dist/v-markdown-editor.css';
import Editor from 'v-markdown-editor'
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import VueSimpleMarkdown from 'vue-simple-markdown'
import 'vue-simple-markdown/dist/vue-simple-markdown.css'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import moment from 'moment'

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY hh:mm')
  }
})

Vue.filter('showNchars', function(value, n) {
  if (value) {
    return String(value).length > n ? `${String(value).substr(0, n)}...` : value
  }
})
 
Vue.use(VueSimpleMarkdown)

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false;

// global register
Vue.use(Editor);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
