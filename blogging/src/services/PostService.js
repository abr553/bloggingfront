import BaseHTTP from "../repository/Repository";

export default class PostService extends BaseHTTP {
  async getAllPosts(params=null) {
    this._setRoute('posts')
    const response = await this.get(null, params)
    return response.data
  }

  async getPostById(id) {
    this._setRoute('posts')
    const response = await this.get(id)
    return response.data
  }

  async createPost(data) {
    this._setRoute('posts')
    const response = await this.save(data)
    return response.data
  }

  async updatePost(id, data) {
    this._setRoute('posts')
    const response = await this.save(data, id)
    return response.data
  }

  async deletePost(id) {
    this._setRoute('posts')
    await this.delete(id)
  }

};
