import PostService from "./PostService";
import UserService from "./UserService";

const postService = new PostService()
const userService = new UserService()

export {
    postService,
    userService
}