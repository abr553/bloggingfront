import BaseHTTP from "../repository/Repository";
import axios from "../config/axios";

export default class PostService extends BaseHTTP {
  async createUser(data) {
    this._setRoute('users')
    const response = await this.save(data)
    return response.data
  }

  async login (data) {
    this._setRoute('auth')
    const response = await this.save(data)
    localStorage.setItem('token', response.data.token)
    localStorage.setItem('user_id', response.data.user_id)
    localStorage.setItem('username', response.data.username)
    axios.defaults.headers.common['Authorization'] = `Token ${response.data.token}`
  }

  logout () {
    localStorage.removeItem('token')
    localStorage.removeItem('user_id')
    localStorage.removeItem('username')
    delete axios.defaults.headers.common['Authorization']
  }
};
