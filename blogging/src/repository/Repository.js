import axios from "../config/axios";

/**
 * BaseHTTP is a class for handler http requests with axios module.
 */
export default class BaseHTTP {
  constructor() {
    this.route = ''
  }

  async get(id=null, params=null) {
    let url = `${this.route}`
    const query = this._queryParams(params)
    if (id) {
      url = `${url}/${id}/`
    }
    url = `${url}${query}`
    return await axios.get(url).then((resp) => {return resp})
  }

  async save(data, id=null) {
    let method = 'post'
    let url = `${this.route}`
    if (id) {
      method = 'patch'
      url = `${url}/${id}`
    }
    url = `${url}/`
    return await axios({
      method,
      data,
      url
    }).then((resp) => {return resp})
  }

  async delete(id) {
    return await axios.delete(`${this.route}/${id}`).then(() => {
      return { data: 'deleted' }
    })
  }

  _setRoute(newRoute) {
    this.route = newRoute
  }

  _queryParams(params={}) {
    if (!params) {
      return ''
    }
    const keys = Object.keys(params)
    const simpleQueryList = keys.map(key => `${key}=${params[key]}`)
    return `?${simpleQueryList.join('&')}`
  }
}